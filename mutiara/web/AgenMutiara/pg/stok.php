  <ol class="breadcrumb">
    <li><a href="?pg=stok"><i class="fa fa-user"></i> Home</a></li>
    <li class="active">Info Stok</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->
<div class="box box-info">
<div class="box-header with-border">
  <h3 class="box-title"><i class="fa fa-bar-chart"></i> Rekap Stok</h3>
</div><!-- /.box-header -->
<div class="box-body">
<table class="table table-condensed">
<tr>
  <th>No</th>
  <th>Kode Barang</th>
  <th>Nama Barang</th>
  <th>Masuk(Kg)</th>
  <th>Keluar(Kg)</th>
  <th>Sisa Stok (Kg)</th>
</tr>
<?php
$agen = $_SESSION['idagen'];
opendb();
$qd = querydb("select kode,nama,harga,sum(stockin) as instok,sum(stockout) as outstok,(sum(stockin) - sum(stockout)) as sisastok from stockagen s
join produk p
on s.idproduk = p.id
where idagen = $_SESSION[idagen]
group by kode");

closedb();
$i = 1;

while($rs = mysql_fetch_array($qd))
{
echo " 
<tr>
  <td>$i</td>
  <td>$rs[kode]</td>
  <td>$rs[nama]</td>
  <td>$rs[instok]</td>
  <td>$rs[outstok]</td>
  <td>$rs[sisastok]</td>
</tr>";
$i++;
} ?>
</table>
</div>
</div><!-- /.box -->
</div>
</div>
  <ol class="breadcrumb">
    <li><a href="?pg=konsumen"><i class="fa fa-user"></i> Administrator</a></li>
    <li class="active">Penjualan</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->
<div class="box box-info">
<div class="box-header with-border">
  <?php 
  if(isset($_POST['addjual']) or isset($_POST['editjual']) or isset($_POST['cekout']) ){
    echo "";
  }else{ 
  ?>
  <div class="pull-right">
      <form method="POST"><button type="submit" name="addjual" class='btn btn-info btn-xs'>+ Tambah Penjualan</button></form>
  </div>
  <?php } ?>
  <h3 class="box-title"><i class="fa fa-credit-card"></i> Data Penjualan</h3>
</div><!-- /.box-header -->
<div class="box-body">
<?php

//Bayar & Cetakstruk
if(isset($_POST['cekout'])){
  $id = $_POST['idjual'];
  $nombayar = $_POST['nombayar'];

  opendb();
  $qlb = querydb("select p.poin,p.nama,p.kode,p.percent,idpenjualan,idproduk,sum(qty) as qty,harga from penjualandetail b
                    join produk p
                    on p.id = b.idproduk
                    where idpenjualan = $id
                    group by idproduk
                     ");
  closedb();
?>
 <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <i class="fa fa-globe"></i> PT. MUTIARA
                <small class="pull-right"><?php echo date("d-M-Y"); ?></small>
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              <address>
                <strong>INVOICE : PA<?php echo $id; ?></strong><br>
                Agen : <?php echo $_SESSION['agen']; ?><br>
              </address>
            </div><!-- /.col -->
          </div><!-- /.row -->
          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th></th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Harga </th>
                    <th>Jumlah(Kg)</th>
                    <th>Disc(%)</th>
                    <th>Poin</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i = 1;
                  $total;
                  while($rs = mysql_fetch_array($qlb)){
                  echo "
                  <tr>
                    <td>$i</td>
                    <td>$rs[kode]</td>
                    <td>$rs[nama]</td>
                    <td>".number_format($rs['harga'])."</td>
                    <td>$rs[qty]</td>
                    <td>$rs[percent]</td>
                    <td>$rs[poin]</td>
                    <td>".number_format(($rs['harga'] * $rs['qty']) - (($rs['harga'] * $rs['qty'])*$rs['percent']/100) )."</td>
                  </tr>";
                  $i++;
                  $total =  ($rsl['harga'] * $rsl['qty']) - (($rsl['harga'] * $rsl['qty'])*$rsl['percent']/100) + $total;    
                }
                  $kembali = $nombayar - $total;
                  ?>
                </tbody>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->
          <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
              <table>
              <tr><td>
              <h5><b>Total Pembelian  </td><td><h5><b>: Rp.<?php echo number_format($total); ?></td></tr>
              <tr><td><h5>Nominal Pembayaran </td><td><h5>: Rp.<?php echo number_format($nombayar); ?></td></tr>
              <tr><td><h5>Kembali  </td><td><h5>: Rp.<?php echo number_format($kembali); ?> </td></tr>
              </h4>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->
          <br>
          <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
              <p class="lead">Payment Methods:</p>
              <img src="dist/img/credit/visa.png" alt="Visa">
              <img src="dist/img/credit/mastercard.png" alt="Mastercard">
              <img src="dist/img/credit/american-express.png" alt="American Express">
              <img src="dist/img/credit/paypal2.png" alt="Paypal">
            </div><!-- /.col -->
          </div><!-- /.row -->
          <br><br>
          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
              <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
            </div>
          </div>
        </section><!-- /.content -->
<?php
//Add penjualan, addbarang,editjual
}else if (isset($_POST['addjual']) or isset($_POST['editjual']) or isset($_POST['addbrg']) or isset($_POST['dellist'])){

  //Dellist
  if(isset($_POST['dellist'])){
    $id = $_POST['id'];
    $idproduk = $_POST['idproduk'];
    opendb();
    querydb("delete from penjualandetail where idpenjualan = $id and idproduk = $idproduk ");
    closedb(); 

  //jika Add penjualan
  }
  if(isset($_POST['addjual'])){
    $id = lasidjual() + 1;

    $agen = $_SESSION['agen'];
    opendb();
    querydb("insert into penjualan (agen,tanggal,status) values ('$agen',now(),1) ");
    closedb();
    
  //Jika Add barang
  }else if(isset($_POST['addbrg'])){

    $id = $_POST['id'];
    $produk = $_POST['idproduk'];
    $qty = $_POST['jml'];
    $sisastok = $_POST['sisastok'];

    if ($sisastok < $qty){
        $errstok = '<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Failed !</h4>
                    Maaf Stok Tidak Mencukupi !
                  </div>';
    }else{
        opendb();
        querydb("insert into penjualandetail (idpenjualan,idproduk,qty) values ($id,$produk,$qty) ");
        closedb();
    }

  //Jika Edit Penjualan
  }else if(isset($_POST['editjual'])){
    $id = $_POST['id'];
  }

  opendb();
  $qlb = querydb("select idpenjualan,id,idproduk,sum(qty) as jml from penjualandetail where idpenjualan =  $id group by idproduk");
  closedb();
?>
<div class="col-md-7">
  <div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Produk</h3>
  </div>
   <div class="box-body">
      <?php 
      opendb();
      $qd = querydb("select * from penjualan where id = $id");
      closedb();
      $rs = mysql_fetch_array($qd);
      ?>
      <blockquote>
        <p>Invoice : PA<?php echo $rs['id']; ?></p>
        <small>Tanggal : <?php echo $rs['tanggal']; ?></small>
      </blockquote>
      <?php echo $errstok; ?>
      <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Nama Barang</th>
          <th>Harga</th>
          <th>Disc</th>
          <th>Stok(Kg)</th>
          <th>Jumlah(Kg)</th>
          <th>Action</th>
        </tr>
        <?php
          opendb();
          $qd = querydb("select p.percent,p.id,kode,nama,harga,(sum(stockin) - sum(stockout)) as sisastok from stockagen s
                        join produk p
                        on s.idproduk = p.id
                        group by kode");
          closedb();
          $i = 1;
          echo "";
          while($rs = mysql_fetch_array($qd)){
          echo "<tr>
                <td>$i</td>
                <td>$rs[nama]</td>
                <td width='100px'>Rp. ".number_format($rs['harga'])."/Kg</td>
                <td width='50px'><center>$rs[percent] %</center></td>
                <td><center>$rs[sisastok]</center></td>
                <td><form method='POST'><input type='number' name='jml' class='form-control' required='required'></td>
                <td>
                      <input type='hidden' name='id' value = $id>
                      <input type='hidden' name='idproduk' value = $rs[id]>
                      <input type='hidden' name='sisastok' value = $rs[sisastok]>
                      <input class='btn btn-info btn-xs' type='submit' name='addbrg' value='+ Add'>
                </form></td>
                </tr>";
            $i++;
          }
        ?>
      </table>
    </div>
  </div>
  </div>

    <!-- List Barang -->
  <div class="col-md-5">
  <div class="box box-info box-solid ">
  <div class="box-header with-border">
  <h3 class="box-title">Keranjang Barang</h3>
  </div>
  <div class="box-body">
    <ul class="nav nav-stacked">
    <form method="POST">
    <?php
    opendb();
    $qlb = querydb("select p.percent,idpenjualan,idproduk,sum(qty) as qty,harga from penjualandetail b
                    join produk p
                    on p.id = b.idproduk
                    where idpenjualan = $id
                    group by idproduk
                     ");
    closedb();
    $total ;
    while($rsl = mysql_fetch_array($qlb))
    {
        echo "
        <li>".namabarang($rsl['idproduk'])." <span class='pull-right badge bg-default'>$rsl[qty] Kg -  Rp. ".number_format(($rsl['harga'] * $rsl['qty']) - (($rsl['harga'] * $rsl['qty'])*$rsl['percent']/100) )."</span>
         <input type='hidden' name='id' value=$rsl[idpenjualan]><input type='hidden' name='idproduk' value=$rsl[idproduk]><button type='submit' class='btn btn-box-tool' name='dellist'><i class='fa fa-times'></i></button></li></form>";
        $total =  ($rsl['harga'] * $rsl['qty']) - (($rsl['harga'] * $rsl['qty'])*$rsl['percent']/100) + $total;     
    }

     echo "<li><h4>Total Penjualan : <div class='pull-right'><b>Rp. ".number_format($total)."</b></div></h4></li>";
     echo "<form method='POST'><li><h4><b>Nominal Bayar(Rp) :</b> <div class='pull-right'><input type='text' pattern='[0-9]*' class='form-control' name='nombayar' required='required'></div></h4></li>";
    ?>
    </ul>
  </div>
  </div>
  </div>
   <div class="col-md-12">
      <hr>
        <input type='hidden' name='idjual' value='<?php echo $id; ?>' >
        <input type='hidden' name='total' value='<?php echo $total; ?>' >
        <input type='submit' name='cekout' class='btn btn-info btn-sm' value='Bayar'>
        <a href='?pg=penjualan'><input type='button' class='btn btn-danger btn-sm' value='Kembali'></a>
     </form>
  </div>
<?php
}else {

?>
<table class="table table-condensed">
<tr>
  <th>No</th>
  <th>Invoice</th>
  <th>Tanggal</th>
  <th>Status</th>
  <th><center>Action</center></th>
</tr>
<?php
opendb();
$qd = querydb("select * from penjualan where agen = '$_SESSION[agen]' order by id desc");
closedb();
$i = 1;
while($rs = mysql_fetch_array($qd))
{
if($rs['status'] == 4 or $rs['status'] == 2){
    $ac = " ";
}else if($rs['status'] == 3){
	$ac = "<center><button type='submit' name='cekbrg' class='btn btn-default btn-xs'><i class='fa  fa-file-text'></i> Cek
	</button></center>";
}else{
    $ac = "<center><button type='submit' name='editjual' class='btn btn-default btn-xs'><i class='fa  fa-edit (alias)'></i> Edit</button> 
        <button class='btn btn-danger btn-xs'>x Delete</button></center>";
}
echo "
<tr>
  <td>$i</td>
  <td>AB$rs[id]</td>
  <td>$rs[tanggal]</td>
  <td>".status($rs['status'])."</td>
  <td>
    <form method='POST'>
        <input type='hidden' name='id' value = $rs[id]>
        <input type='hidden' name='tanggal' value = '$rs[tanggal]'>
        $ac
       </form>
    </td>
</tr>";
$i++;
} ?>
</table>
<?php } ?>
</div>
</div><!-- /.box -->
</div>
</div>
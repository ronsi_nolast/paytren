  <ol class="breadcrumb">
    <li><a href="?pg=konsumen"><i class="fa fa-user"></i> Administrator</a></li>
    <li class="active">Belanja</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->
<div class="box box-info">
<div class="box-header with-border">
  <?php 
  if(isset($_POST['addbelanja']) or isset($_POST['editbelanja']) ){
    echo "";
  }else{ 
  ?>
  <div class="pull-right">
      <form method="POST"><button type="submit" name="addbelanja" class='btn btn-info btn-xs'><i class='fa fa-plus-square'></i> Tambah Belanja</button></form>
  </div>
  <?php } ?>
  <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Data Belanja</h3>
</div><!-- /.box-header -->
<div class="box-body">
<?php
if(isset($_POST['detail'])){
$id = $_POST['id'];

opendb();
$qm = querydb("select * from belanja where id = $id");
closedb();

$rm = mysql_fetch_array($qm);
?>
<section class="invoice">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> PT MUTIARA
          <small class="pull-right"><?php echo date('d M Y'); ?></small>
        </h2>
      </div><!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
          <strong><?php echo $_SESSION['agen']; ?></strong><br>
          Tanggal Order : <?php echo $rm['tanggal'];?><br>
          Surat Jalan : <b>SJ<?php echo $rm['sj'];?></b><br>
          Tanggal Kirim : <?php echo $rm['tglpengiriman'];?><br>
          Status: <?php echo status($rm['status']);?><br>
        </address>
      </div><!-- /.col -->
      <div class="col-sm-5 invoice-col">
        <address>
          <strong>Data Pembayaran </strong><br>
          Bank Pengirim: <?php echo $rm['bank'];?><br>
          Atasnama : <?php echo $rm['anama'];?><br>
          No Rek: <?php echo $rm['rek'];?> , Nominal: Rp. <?php echo number_format($rm['nominal']);?><br>
          Tanggal: <?php echo $rm['tgltrf'];?> , Jam Transfer : <?php echo $rm['jamtrf'];?><br>
        </address>
      </div>
    </div><!-- /.row -->
    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Produk</th>
              <th>Nama produk</th>
              <th>Harga (Rp.)</th>
              <th>Discount(%)</th>
              <th>Poin</th>
              <th>Qty</th>
              <th>Subtotal(Rp.)</th>
            </tr>
          </thead>
          <tbody>
          <?php 
            opendb();
            $qp = querydb("select p.poin,p.nama,p.kode,p.percent,idbelanja,hp.idproduk,sum(qty) as qty,hp.harga from belanjadetail b
                    join produk p
                    on p.id = b.idproduk
                    join hargaproduk hp
                    on p.id = hp.idproduk
                    where idbelanja = $id
                    group by idproduk");
            closedb();
            $i = 1;
            $total = 0;
            while ($rp = mysql_fetch_array($qp))
            {
              echo "<tr>
                    <td>$i</td>
                    <td>$rp[kode]</td>
                    <td>$rp[nama]</td>
                    <td>".number_format($rp['harga'])."</td>
                    <td>$rp[percent]</td>
                    <td>$rp[poin]</td>
                    <td>$rp[qty]</td>
                    <td>".number_format(($rp['harga'] * $rp['qty'])-(($rp['harga'] * $rp['qty'])*$rp['percent']/100))."</td>        
              </tr>";
              $total =  (($rp['harga'] * $rp['qty'])-(($rp['harga'] * $rp['qty'])*$rp['percent']/100)) + $total;  
            $i++;
            }
          ?>
          </tbody>
        </table>
        <div class="pull-right"><h4><b>Rp. <?php echo number_format($total); ?></b></h4></div>
      </div><!-- /.col -->
    </div><!-- /.row -->
   <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="?pg=appstok" class="btn btn-danger btn-xs"> Kembali</a>
      </div>
    </div>
</section>
<?php
}else if(isset($_POST['terima'])){
$id = $_POST['idbelanja'];
$agen = $_SESSION['agen'];

opendb();
querydb("insert into stockagen (idagen,idproduk,tanggal,iddoc,stockin,type) select ('$agen') as a ,idproduk,now(),idbelanja,qty,1 from belanjadetail where idbelanja = $id");
querydb("update belanja set status = 4 where id = $id");
closedb();
echo "<div class='callout callout-info'><h4>Success !!</h4><p>Data Belanja Invoice : AB$id  Berhasil Diterima! <a href='?pg=belanja'><button type='button' class='btn btn-danger btn-xs'>Cek stok</button</a></p></div>";

}else if(isset($_POST['lbayar'])){
  $id = $_POST['id'];
  opendb();
  querydb("update belanja set bank='$_POST[bank]',anama='$_POST[anama]',rek='$_POST[norek]',tgltrf='$_POST[tgltrf]',jamtrf='$_POST[jamtrf]',alamat='$_POST[alamat]',kota='$_POST[kota]',
  totbelanja = '$_POST[total]',status = 2,nominal = $_POST[nominal] where id = $id ");
  closedb();
  echo "<div class='callout callout-info'><h4>Success !!</h4><p>Konfirmasi pembayaran Invoice : AB$id  Berhasil dikonfirmasikan !! <a href='?pg=belanja'><button type='button' class='btn btn-danger btn-xs'>Ok</button</a></p></div>";

//Konfirmasi Pembayaran
}else if(isset($_POST['cekout'])){
  $id = $_POST['idbelanja'];
  $total = $_POST['total'];
?>
<form class="form-horizontal" method="POST">
  <div class="box-body">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Invoice</label>
      <div class="col-sm-3">
        <input type="text" class="form-control input-sm"  value='AB<?php echo $id; ?>' disabled>
      </div>
      <div class="col-sm-4">
        <input type="text" class="form-control input-sm"  value='Total Belanja : Rp.<?php echo number_format($total); ?>' disabled>
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Data Transfer</label>
      <div class="col-sm-3">
        <input type="number" name = 'nominal' class="form-control input-sm" placeholder="Isikan nominal transfer..."  required="required">
      </div>
      <div class="col-sm-3">
        <input type="date" name = 'tgltrf' class="form-control input-sm"  required="required">
      </div>
      <div class="col-sm-2">
        <input type="text" name = 'jamtrf' class="form-control input-sm" placeholder="Jam Transfer..."  required="required">
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Alamat</label>
      <div class="col-sm-10">
        <input type="text" name='alamat' class="form-control input-sm"  placeholder="Isikan Alamat Pengiriman Barang.." required="required">
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Kota/Kab</label>
      <div class="col-sm-6">
        <select name='kota' class="form-control input-sm"> 
        <?php
        opendb();
        $qd = querydb("select * from all_kabkot order by nama_kabkot asc ");
        closedb();
        while ($rs = mysql_fetch_array($qd)){
          echo "<option value='$rs[nama_kabkot]'>$rs[nama_kabkot]</option>";
        }
        ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Data Bank Pengirim</label>
      <div class="col-sm-3">
        <input type="text" class="form-control input-sm" name='norek' placeholder="Isikan no rekening .."  required="required">
      </div>
      <div class="col-sm-3">
        <input type="text" class="form-control input-sm"  name='bank' placeholder="Isikan nama bank .." required="required">
      </div>
       <div class="col-sm-3">
        <input type="text" class="form-control input-sm"  name='anama' placeholder="Atas nama .." required="required">
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Data Bank Tujuan</label>
      <div class="col-sm-3">
        <input type="text" class="form-control input-sm" name='norekt' placeholder="Isikan no rekening .."  >
      </div>
      <div class="col-sm-3">
        <input type="text" class="form-control input-sm"  name='bankt' placeholder="Isikan nama bank .." >
      </div>
       <div class="col-sm-3">
        <input type="text" class="form-control input-sm"  name='anamat' placeholder="Atas nama .." >
      </div>
    </div>
  </div><!-- /.box-body -->
  <div class="box-footer">
    <input type="hidden" name='id' value='<?php echo $id; ?>'> 
    <input type="hidden" name='total' value='<?php echo $total; ?>'> 
    <button type="submit" name="lbayar" class="btn btn-info btn-xs">Lanjutkan</button>
    <a href='?pg=belanja'><button type='button' class="btn btn-danger btn-xs">Batal</button></a>
  </div><!-- /.box-footer -->
</form>
<?php
//Tambah keranjang Belanja, Edit Belanja
}else if(isset($_POST['addbelanja']) or isset($_POST['editbelanja']) or isset($_POST['addbrg']) or isset($_POST['dellist'])){

  //jika tambah belanja
  if (isset($_POST['addbelanja'])){
    $id = lasidbelanja() + 1;
    opendb();
    querydb("insert into belanja (agen,tanggal,status) values('$_SESSION[agen]',now(),1)");
    closedb();

  //jika Edit Belanja atau cek barang
  }else if (isset($_POST['editbelanja']) or isset($_POST['cekbrg'])){
    $id = $_POST['id'];

  //Jika Add Barang  
  }else if (isset($_POST['addbrg'])){
    $id = $_POST['id'];
    opendb();
    querydb("insert into belanjadetail (idbelanja,idproduk,qty) values($id,$_POST[idbrg],$_POST[jml]) ");
    closedb();

  //Hapust list produk
  }else if (isset($_POST['dellist'])){
    $id = $_POST['id'];
    $idproduk = $_POST['idproduk'];
    opendb();
    querydb("delete from belanjadetail where idbelanja = $id and idproduk = $idproduk ");
    closedb();

  }
    //View List Barang detail
    opendb();
    $qlb = querydb("select idbelanja,id,idproduk,sum(qty) as jml from belanjadetail where idbelanja =  $id group by idproduk");
    closedb();

?>
<div class="col-md-7">
  <div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Produk</h3>
  </div>
   <div class="box-body">
      <?php 
      opendb();
      $qd = querydb("select id,DATE_FORMAT(tanggal,'%d %M %Y') as tanggal from belanja where id = $id");
      closedb();
      $rs = mysql_fetch_array($qd);
      ?>
      <blockquote>
        <p>Invoice : AB<?php echo $rs['id']; ?></p>
        <small>Tanggal : <?php echo $rs['tanggal']; ?></small>
      </blockquote>
      <table class="table table-hover">
        <tr>
          <th>No</th>
          <th>Nama Barang</th>
          <th>Harga</th>
          <th>Jumlah</th>
          <th>Action</th>
        </tr>
        <?php
          opendb();
          $qd = querydb("select p.id,nama,poin,percent,h.harga,masapoin,masadisc from produk p join hargaproduk h on h.idproduk = p.id where h.idkota = $_SESSION[kota]");
          closedb();
          $i = 1;
          echo "";
          while($rs = mysql_fetch_array($qd)){

          echo "<tr>
                
                <td>$i</td>
                <td>$rs[nama]<br>Discount : <b>$rs[percent]%</b> <br> <h4><b>+ $rs[poin] Poin</b></h4> </td>
                <td width='100px'>Rp. ".number_format($rs['harga'])."/Kg</td>
                <td><form method='POST'><input type='number' name='jml' class='form-control input-sm' required='required'></td>
                <td>
                      <input type='hidden' name='idbrg' value = $rs[id]>
                      <input type='hidden' name='id' value = $id>
                      <input class='btn btn-info btn-xs' type='submit' name='addbrg' value='+ Add'>
                </form></td>
                </tr>";
            $i++;
          }
        ?>
      </table>
    </div>
  </div>
  </div>
  <!-- List Barang -->
  <div class="col-md-5">
  <div class="box box-info box-solid ">
  <div class="box-header with-border">
  <h3 class="box-title">Keranjang Barang</h3>
  </div>
  <div class="box-body">
    <ul class="nav nav-stacked">
    <?php
    opendb();
    $qlb = querydb("select p.percent,idbelanja,hp.idproduk,sum(qty) as qty,hp.harga from belanjadetail b
                    join produk p
                    on p.id = b.idproduk
                    join hargaproduk hp
                    on p.id = hp.idproduk
                    where idbelanja = $id
                    group by idproduk
                     ");
    closedb();
    $total = 0 ;
    while($rsl = mysql_fetch_array($qlb))
    {
    	if(isset($_POST['cekbrg'])){
    	$d = "";
    	}else{
    	$d = "<button type='submit' class='btn btn-box-tool' name='dellist'><i class='fa fa-times'></i></button>";
    	}
        echo "
        <li><form method='POST'>".namabarang($rsl['idproduk'])." <span class='pull-right badge bg-default'>$rsl[qty] Kg -  Rp. ".number_format(($rsl['harga'] * $rsl['qty'])-(($rsl['harga'] * $rsl['qty'])*$rsl['percent']/100))."</span>
         <input type='hidden' name='id' value=$rsl[idbelanja]><input type='hidden' name='idproduk' value=$rsl[idproduk]>$d</form></li>";
        $total =  (($rsl['harga'] * $rsl['qty'])-(($rsl['harga'] * $rsl['qty'])*$rsl['percent']/100)) + $total;     
    }

    echo "<li><h4>Total Belanja : <div class='pull-right'><b>Rp. ".number_format($total)."</b></div></h4></li>";
    
    if(isset($_POST['cekbrg'])){
    $b = "<input type='submit' name='terima' class='btn btn-info btn-xs' value='Terima'> ";
    }else{
    $b = "<input type='submit' name='cekout' class='btn btn-info btn-xs' value='Bayar'> ";
    }
    ?>
    </ul>
  </div>
  </div>
  </div>
  <div class="col-md-12">
      <hr>
     <form method='POST'>
        <input type='hidden' name='idbelanja' value='<?php echo $id; ?>' >
        <input type='hidden' name='total' value='<?php echo $total; ?>' >
        <?php echo $b; ?>
        <a href='?pg=belanja'><input type='button' class='btn btn-danger btn-xs' value='Kembali'></a>
     </form>
  </div>

<?php }else{ ?>
<table class="table table-hover">
<tr>
  <th>No</th>
  <th>Invoice</th>
  <th>Tanggal</th>
  <th>Action</th>
</tr>
<?php
opendb();
$qd = querydb("select sj,id,status,DATE_FORMAT(tanggal,'%d %M %Y') as tanggal,DATE_FORMAT(tglpengiriman,'%d %M %Y') as tglpengiriman,DATE_FORMAT(tglterima,'%d %M %Y') as tglterima from belanja where agen = '$_SESSION[agen]' order by id desc");
closedb();
$i = 1;
while($rs = mysql_fetch_array($qd))
{
if($rs['status'] == 4 or $rs['status'] == 2){
    $ac = "<button type='submit' name='detail' class='btn btn-default btn-xs'><i class='fa  fa-file-text'></i> Detail  </button>";
}else if($rs['status'] == 3){
	$ac = "<button type='submit' name='cekbrg' class='btn btn-default btn-xs'><i class='fa  fa-file-text'></i> Cek	</button>";
}else{
    $ac = "<button type='submit' name='editbelanja' class='btn btn-default btn-xs'><i class='fa  fa-edit (alias)'></i> Edit</button> 
        <button class='btn btn-danger btn-xs'>x Delete</button>";
}
echo "
<tr>
  <td>$i</td>
  <td><b>AB$rs[id] </b><br>Surat Jalan : SJ$rs[sj] <br>Status : ".status($rs['status'])."<br></td>
  <td>Tanggal Order: $rs[tanggal]<br>Tanggal Kirim : $rs[tglpengiriman]<br>Tanggal Terima : $rs[tglterima]</td>
  <td><form method='POST'><br>
        <input type='hidden' name='id' value = $rs[id]>
        <input type='hidden' name='tanggal' value = '$rs[tanggal]'>
        $ac
  </form></td>
</tr>";
$i++;
} ?>
</table>
<?php } ?>
</div>
</div><!-- /.box -->
</div>
</div>
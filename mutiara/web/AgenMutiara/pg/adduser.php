
  <ol class="breadcrumb">
    <li><a href="?pg=adduser"><i class="fa fa-user"></i> Administrator</a></li>
    <li class="active">Add user</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-6">
<!-- Horizontal Form -->
<div class="box box-success">
<div class="box-header with-border">
  <h3 class="box-title">Input User</h3>
</div><!-- /.box-header -->

<!-- Add User -->
<?php
if (isset($_POST['input'])){
  if ($_POST['pass'] <> $_POST['passcnf']){
        echo "<br><div class='col-md-12'><div class='callout callout-danger'><h4>Failed !!</h4><p>Pengulangan konfirmasi password tidak sama !!</p></div></div><hr>";
  }else{
        $passenc = md5($_POST['pass']);
        opendb();
        querydb("insert into t_user (username,password,tglinput) values('$_POST[user]','$passenc',now())");
        closedb();
        echo "<br><div class='col-md-12'><div class='callout callout-success'><h4>Success !!</h4><p>User Berhasil ditambahkan !!</p></div></div><hr>";
  }
}
?>
<!-- /Add User -->

<!-- form start -->
<form class="form-horizontal" method="POST">
  <div class="box-body">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
      <div class="col-sm-10">
        <input type="text" name='user' class="form-control"  placeholder="Username" required="required">
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
      <div class="col-sm-10">
        <input type="password" name='pass' class="form-control"  placeholder="Password" required="required">
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Konfirmasi Password</label>
      <div class="col-sm-10">
        <input type="password" name='passcnf' class="form-control"  placeholder=" Konfirmasi Password" required="required">
      </div>
    </div>
  </div><!-- /.box-body -->
  <div class="box-footer">
    <button type="submit" name="input" class="btn btn-success pull-right">Add</button></a>
  </div><!-- /.box-footer -->
</form>
<hr>
</div><!-- /.box -->
</div>
<div class="col-md-5">
<!-- Horizontal Form -->
<div class="box box-success">
<div class="box-header with-border">
  <h3 class="box-title">Tabel User</h3>
</div><!-- /.box-header -->
<table class="table table-condensed">
<tr>
  <th>No</th>
  <th>Username</th>
  <th>Action</th>
</tr>
<?php
opendb();
$qd = querydb("select * from t_user order by username asc");
closedb();
$i = 1;
while($rs = mysql_fetch_array($qd))
{
echo "
<tr>
  <td>$i</td>
  <td>$rs[username]</td>
  <td><button class='btn btn-default btn-xs'>Edit</button> <button class='btn btn-default btn-xs'>Delete</button></td>
</tr>";
$i++;
} ?>
</table>
<hr>
</div><!-- /.box -->
</div>
</div>
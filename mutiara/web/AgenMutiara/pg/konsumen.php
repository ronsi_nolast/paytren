  <ol class="breadcrumb">
    <li><a href="?pg=konsumen"><i class="fa fa-user"></i> Administrator</a></li>
    <li class="active">Konsumen</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->
<div class="box box-info">
<div class="box-header with-border">
  <?php 
  if(!isset($_POST['addkonsumen'])){
  ?>
  <div class="pull-right">
      <form method="POST"><button type="submit" name="addkonsumen" class='btn btn-info btn-xs'><i class='fa fa-plus-square'></i> Tambah Konsumen</button></form>
  </div>
  <?php } ?>
  <h3 class="box-title">Data Konsumen</h3>
</div><!-- /.box-header -->
<div class="box-body">
<?php
if (isset($_POST['inskonsumen'])){
  opendb();
  querydb("insert into konsumen(nama,alamat,kota,hp,agen,date) values('$_POST[nama]','$_POST[alamat]','$_POST[kota]','$_POST[hp]','$_SESSION[agen]',now()) ");
  closedb();
  echo "<br><div class='col-md-6'><div class='callout callout-info'><h4>Success !!</h4><p>Data Konsumen Berhasil ditambahkan !!</p></div></div>";
}else if (isset($_POST['addkonsumen'])){
?>
<form class="form-horizontal" method="POST">
  <div class="box-body">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>
      <div class="col-sm-6">
        <input type="text" name='nama' class="form-control input-sm"  placeholder="isikan nama lengkap konsumen..." required="required">
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Alamat</label>
      <div class="col-sm-10">
        <input type="text" name='alamat' class="form-control input-sm"  placeholder="Isikan Alamat lengkap konsumen..." required="required">
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Kota</label>
      <div class="col-sm-6">
        <select name='kota' class="form-control input-sm"> 
        <?php
        opendb();
        $qd = querydb("select * from all_kabkot order by nama_kabkot asc ");
        closedb();
        while ($rs = mysql_fetch_array($qd)){
          echo "<option value='$rs[nama_kabkot]'>$rs[nama_kabkot]</option>";
        }
        ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">No. HP</label>
      <div class="col-sm-6">
        <input type="text" name='hp' class="form-control input-sm"  placeholder="Isikan no hp konsumen...." required="required">
      </div>
    </div>
  </div><!-- /.box-body -->
  <div class="box-footer">
    <button type="submit" name="inskonsumen" class="btn btn-info btn-xs">Submit</button>
    <a href='?pg=konsumen'><button type='button' class="btn btn-danger btn-xs">Batal</button></a>
  </div><!-- /.box-footer -->
</form>
<?php } ?>
<table class="table table-hover">
<tr>
  <th>No</th>
  <th>ID</th>
  <th>Alamat</th>
  <th>No. HP</th>
  <th>Tanggal Gabung</th>
  <th>Action</th>
</tr>
<?php
opendb();
$qd = querydb("select * from konsumen order by nama asc");
closedb();
$i = 1;
while($rs = mysql_fetch_array($qd))
{
echo "
<tr>
  <td>$i</td>
  <td>KM$rs[id]- $rs[nama]</td>
  <td>$rs[alamat]</td>
  <td>$rs[hp]</td>
  <td>$rs[date]</td>
  <td><button class='btn btn-default btn-xs' ><i class='fa fa-edit (alias)'></i> Edit</button> <button class='btn btn-danger btn-xs'>x Delete</button></td>
</tr>";
$i++;
} ?>
</table>
</div>
</div><!-- /.box -->
</div>
</div>
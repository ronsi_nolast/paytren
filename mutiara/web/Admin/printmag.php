<?php
session_start();
include 'dbfunction.php';
include 'config.php';
include 'function.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PT. MUTIARA | Cetak Mutasi Gudang</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body onload="window.print();">
    <div class="wrapper">
      <!-- Main content -->
    <?php
    //Detail invoice or Print
     $id = $_GET['id'];

     opendb();
     $qm = querydb("select id,idgudangtujuan,sj,DATE_FORMAT(tglkirim,'%d %M %Y') as tglkirim,docno from mutasi where id = $id");
     closedb();

     $rm = mysql_fetch_array($qm);
   ?>
    <section class="invoice">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> PT MUTIARA
          <small class="pull-right"><?php echo date('d M Y'); ?></small>
        </h2>
      </div><!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
          Dari: 
          <strong><?php echo namagudang($_SESSION['gudang']); ?></strong><br>
          Gudang Tujuan : <?php echo namagudang($rm['idgudangtujuan']);?><br>
          No. Document : <?php echo $rm['docno'];?><br>
          Surat Jalan : <b>SJ<?php echo $rm['id'];?></b><br>
          Tanggal Kirim : <?php echo $rm['tglkirim'];?><br>
        </address>
      </div><!-- /.col -->
    </div><!-- /.row -->
    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Produk</th>
              <th>Nama produk</th>
              <th>Qty</th>
            </tr>
          </thead>
          <tbody>
          <?php 
            opendb();
            $qp = querydb("select kode,nama,sum(qty) as qty from mutasidetail ms join produk p on ms.idproduk = p.id where ms.idmutasi = $id group by ms.idproduk ");
            closedb();
            $i = 1;
            while ($rp = mysql_fetch_array($qp))
            {
              echo "<tr>
                    <td>$i</td>
                    <td>$rp[kode]</td>
                    <td>$rp[nama]</td>
                    <td>$rp[qty]</td>       
              </tr>";
            $i++;
            }
          ?>
          </tbody>
        </table>
      </div><!-- /.col -->
    </div><!-- /.row -->
   <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="print.php?id=<?php echo $id; ?>" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-print"></i> Print</a>
      </div>
    </div>
  </section><!-- /.content -->
    </div><!-- ./wrapper -->
  </body>
</html>

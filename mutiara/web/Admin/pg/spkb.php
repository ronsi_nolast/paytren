<ol class="breadcrumb">
    <li><a href="?pg=konsumen"><i class="fa fa-user"></i> Administrator</a></li>
    <li class="active">SPKB (Surat perintah kirim barang)</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->
<div class="box box-success">
<div class="box-header with-border">
<h3 class="box-title"><i class="fa fa-pencil-square-o"></i> SPKB (Surat perintah kirim barang)</h3>
<div class="box-body">

<?php 

//Proses SJ ( SUrat Jalan)
if(isset($_POST['prosessj'])){

	$id = $_POST['id'];
	$tglkirim = $_POST['tglkirim'];
	$sjno = $_POST['spkbno'];

	opendb();
	querydb("update spkb set status = 2,tglkirim = '$tglkirim',sjno = $sjno where spkbno = $sjno");
	closedb();

	echo "<div class='callout callout-success'><h4>Success !!</h4><p>SPKB Berhasil di proses !!</p></div>";

}else if(isset($_POST['detilb'])){
	$id = $_POST['invoiceno'];
	$spkb = $_POST['spkbno'];

	opendb();
	$qb = querydb("select * from belanja where id = $id");
	closedb();
	$rb = mysql_fetch_array($qb);
?>
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="fa fa-globe"></i> PT. MUTIARA
        <small class="pull-right"><?php echo date("d-M-Y"); ?></small>
      </h2>
    </div><!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      <address>
        <strong>SPKB : SPKB<?php echo $spkb; ?></strong><br>
        INVOICE : AB<?php echo $_POST['invoiceno']; ?><br>
        Surat Jalan : SJ<?php echo $_POST['invoiceno']; ?><br>
      </address>
    </div>
    <div class="col-sm-8 invoice-col">
      <address>
        <strong>Agen :<?php echo $rb['agen']; ?></strong><br>
        Alamat : <?php echo $rb['alamat']; ?><br>
        Kota : <?php echo $rb['kota']; ?>
      </address>
    </div><!-- /.col -->
  </div><!-- /.row -->
  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Harga </th>
            <th>Qty(Kg)</th>
            <th>Disc(%)</th>
            <th>Poin</th>
            <th>Subtotal</th>
          </tr>
        </thead>
        <tbody>
          <?php
          	$kodegudang = kodegudang($_SESSION['gudang']);
		    opendb();
		    $qlb = querydb("select DATE_FORMAT(s.tglkirim,'%d %M %Y') as tglkirim,s.status,p.harga,p.poin,p.percent,p.kode,p.nama,idproduk,qty,sjno from spkb s
							join spkbdetail sp
							on s.spkbno = sp.spkbno
							join produk p
							on p.id = sp.idproduk
							where s.invoiceno = $id and s.kodegudang = '$kodegudang'
		                     ");
		      closedb();
		      $total ;
              $i = 1;
              $total;
              while($rs = mysql_fetch_array($qlb)){

              $spkno = spkbno($id,$rs['idproduk']);
              $spkbgudang = spkbgudang($id,$rs['idproduk']);

              echo "
              <tr>
              	<form method='POST'>
                <td>$i</td>
                <td>$rs[kode]</td>
                <td>$rs[nama]</td>
                <td>".number_format($rs['harga'])."</td>
                <td>$rs[qty]</td>
                <td>$rs[percent]</td>
                <td>$rs[poin]</td>
                <td>".number_format(($rs['harga'] * $rs['qty']) - (($rs['harga'] * $rs['qty'])*$rs['percent']/100) )."</td>
              </tr>";
              $i++;
              $tglkirim =  $rs['tglkirim']; 
              $status =  $rs['status'];     
            }

          ?>
        </tbody>
      </table>
      <form method='POST'>
      <div class="form-group">
      <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Pengiriman</label> :
      <?php
      if( $status == 1){
      echo "
      <div class='col-sm-3'>
        <input type='date' name='tglkirim' class='form-control'  required='required'>
      </div>";
  	  }else{
  	  	echo $tglkirim;
  	  }
      ?>
    </div>
    </div><!-- /.col -->
  </div><!-- /.row -->
  <br>
  <br><br>
  <hr>
  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <input type='hidden' name='id' value=<?php echo $id; ?> >
      <input type='hidden' name='spkbno' value=<?php echo $spkb; ?> >
      <?php
      if($status == 1){
      echo "<input type='submit' name='prosessj' class='btn btn-success btn-sm' value='Proses SJ'>";
      }
      ?>
      <a href="?pg=spkb" class="btn btn-danger btn-sm">Kembali</a>
    </div>
  </div>
  </form>
</section><!-- /.content -->

<?php
}else{
?>
<table class="table table-condensed">
<tr>
  <th>No</th>
  <th>No.SPKB</th>
  <th>No.Surat Jalan</th>
  <th>Invoice</th>
  <th>Tanggal SPKB</th>
  <th>Tanggal Pengiriman</th>
  <th>Status</th>
  <th><center>Action</center></th>
</tr>
<?php
$kodeg = kodegudang($_SESSION['gudang']);
opendb();
$qd = querydb("select * from spkb where kodegudang = '$kodeg' order by spkbno desc");
closedb();
$i = 1;
while($rs = mysql_fetch_array($qd))
{
    $ac = "<center><button type='submit' name='detilb' class='btn btn-default btn-xs'>Detil</button> 
        <button class='btn btn-danger btn-xs'>Delete</button></center>";
echo "
<tr>
  <td>$i</td>
  <td>SPKB$rs[spkbno]</td>
  <td>SJ$rs[sjno]</td>
  <td>AB$rs[invoiceno]</td>
  <td>$rs[tanggal]</td>
  <td>$rs[tglkirim]</td>
  <td>".statusspkb($rs['status'])."</td>
  <td>
    <form method='POST'>
        <input type='hidden' name='spkbno' value = $rs[spkbno]>
        <input type='hidden' name='invoiceno' value = $rs[invoiceno]>
        $ac
       </form>
    </td>
</tr>";
$i++;
} ?>
</table>
<?php } ?>
</div>
</div><!-- /.box -->
</div>
</div>

  <ol class="breadcrumb">
    <li><a href="?pg=adduser"><i class="fa fa-database"></i> Master Data</a></li>
    <li class="active">Data Barang</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->
<div class="box box-success">
<div class="box-header with-border">
  <div class="pull-right">
      <form method="POST"><button type="submit" name="addp" class='btn btn-success btn-xs'><i class='fa fa-plus-square'></i> Tambah Barang</button></form>
  </div>
  <h3 class="box-title">Data Barang</h3>
</div><!-- /.box-header -->
<div class="box-body">
<?php
//Update Data barang
if(isset($_POST['updatebarang'])){
  $id = $_POST['id'];
  $harga = $_POST['harga'];
  $disc = $_POST['disc'];
  $poin = $_POST['poin'];
  $mpoin = $_POST['mpoin'];
  $mdisc = $_POST['mdisc'];
  $safetystock = $_POST['safetystock'];

  opendb();
  querydb("update produk set masapoin='$mpoin',masadisc='$mdisc',harga=$harga,percent=$disc,safetystock=$safetystock,poin=$poin where id = $id");
  closedb();

  echo "<br><div class='col-md-12'><div class='alert alert-success'>Success, Barang Berhasil Di update !!</div></div>";

//Insert Data Barang
}else if(isset($_POST['inputbrg'])){
  $kp = acaka(5);
  opendb();
  querydb("insert into produk (kode,nama,satuan,safetystock,idkategori,poin,percent,masapoin,masadisc,harga) 
           values('KP$kp','$_POST[nbarang]','$_POST[satuan]',$_POST[stokminim],$_POST[kategori],$_POST[poin],$_POST[disc],'$_POST[mpoin]','$_POST[mdisc]',$_POST[harga] )");
  closedb();
  echo "<br><div class='col-md-12'><div class='alert alert-success'>Success, Barang Berhasil Diinput !!</div></div>";
?>
<?php
//Form Add Barang
}else if(isset($_POST['addp'])){
  ?>
<form class="form-horizontal" method="POST">
  <div class="box-body">
    <div class="form-group">
      <label class="col-sm-2 control-label">Nama Barang</label>
      <div class="col-sm-6">
        <input type="text" name='nbarang' class="form-control input-sm"  placeholder="nama barang" required="required">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Kategori</label>
      <div class="col-sm-6">
        <select name="kategori" class="form-control input-sm">
          <?php
            opendb();
            $qd = querydb("select * from kategori");
            closedb();
            while ($rs = mysql_fetch_array($qd)){
                echo "<option value='$rs[id]'>$rs[nama]</option>";
            }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Satuan</label>
      <div class="col-sm-4">
        <select name="satuan" class="form-control input-sm">
          <option value="Kilogram">Kilogram</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Harga (Rp)</label>
      <div class="col-sm-3">
        <input type="number" name='harga' class="form-control input-sm"  placeholder="Isikan Harga barang" required="required">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Stok Minimal</label>
      <div class="col-sm-3">
        <input type="number" name='stokminim' class="form-control input-sm"  placeholder="Stok minimal barang..." required="required">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Discount(%)</label>
      <div class="col-sm-3">
        <input type="number" name='disc' class="form-control input-sm"  placeholder="Isikan Jumlah discount " required="required">
      </div>
      <div class="col-sm-3">
       Berlaku Discount <input type="date" name='mdisc' class="form-control input-sm" required="required">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Poin(*)</label>
      <div class="col-sm-3">
        <input type="number" name='poin' class="form-control input-sm"  placeholder="Isikan Jumlah Poin " required="required">
      </div>
      <div class="col-sm-3">
        Berlaku poin<input type="date" name='mpoin' class="form-control input-sm"  required="required">
      </div>
    </div>
  </div><!-- /.box-body -->
  <div class="box-footer">
    <button type="submit" name="inputbrg" class="btn btn-success btn-xs">+ Submit </button> <a href="?pg=barang"><button type="button" name="input" class="btn btn-danger btn-xs"> x Batal</button></a>
  </div><!-- /.box-footer -->
</form>
<?php
}
?>
<br>
<div class="box-body table-responsive no-padding">
<table class="table table-hover">
<tr>
  <th>No</th>
  <th>kode</th>
  <th>Nama</th>
  <th>Harga</th>
  <th>Disc(%)</th>
  <th>Poin</th>
  <th>Stok Minimal</th>
  <th>Action</th>
</tr>
<?php
opendb();
$qd = querydb("select * from produk order by nama asc");
closedb();
$i = 1;
while($rs = mysql_fetch_array($qd))
{
echo "
<tr>
  <td>$i</td>
  <td>$rs[kode]</td>
  <td>$rs[nama]</td>
  <td><form method='POST'>Rp. <input type='text' class='form-control input-sm' name='harga' value='$rs[harga]' required='required'></td>
  <td>%.<input type='number' class='form-control input-sm' name='disc' value='$rs[percent]' required='required'>
        Berlaku Disc(%)<input type='date' class='form-control' name='mdisc' value='$rs[masadisc]' required='required'></td>
  <td>*<input type='number' class='form-control input-sm' name='poin' value='$rs[poin]' required='required'>
       Berlaku Poin<input type='date' class='form-control' name='mpoin' value='$rs[masapoin]' required='required'></td>
  <td>Kg.<input type='text' class='form-control input-sm' name='safetystock' value='$rs[safetystock]' required='required'></td>
  <td><center><input type='hidden' name='id' value=$rs[id]><button type='submit' class='btn btn-default btn-xs' name='updatebarang'><i class='fa fa-edit (alias)'></i> Update</button> 
      <button class='btn btn-danger btn-xs'>Delete</button></form></center></td>
</tr>";
$i++;
} ?>
</table>
</div>
</div><!-- /.box -->
</div>
</div>
</div>
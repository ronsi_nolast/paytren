
  <ol class="breadcrumb">
    <li><a href="?pg=adduser"><i class="fa fa-database"></i> Master Data</a></li>
    <li class="active">Approve Mutasi</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->
<div class="box box-success">
<div class="box-header with-border">
  <h3 class="box-title">Penerimaan Mutasi (Gudang)</h3>
</div><!-- /.box-header -->
<div class="box-body">
<?php
//Lihat Invoice
if(isset($_POST['detil'])){
      $id = $_POST['idmut'];

     opendb();
     $qm = querydb("select id,idgudangtujuan,sj,DATE_FORMAT(tglkirim,'%d %M %Y') as tglkirim,docno from mutasi where id = $id");
     closedb();

     $rm = mysql_fetch_array($qm);
?>
<section class="invoice">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> PT MUTIARA
          <small class="pull-right"><?php echo date('d M Y'); ?></small>
        </h2>
      </div><!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
          Dari: 
          <strong><?php echo namagudang($_SESSION['gudang']); ?></strong><br>
          Gudang Tujuan : <?php echo namagudang($rm['idgudangtujuan']);?><br>
          No. Document : <?php echo $rm['docno'];?><br>
          Surat Jalan : <b>SJ<?php echo $rm['id'];?></b><br>
          Tanggal Kirim : <?php echo $rm['tglkirim'];?><br>
        </address>
      </div><!-- /.col -->
    </div><!-- /.row -->
    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Produk</th>
              <th>Nama produk</th>
              <th>Qty</th>
            </tr>
          </thead>
          <tbody>
          <?php 
            opendb();
            $qp = querydb("select kode,nama,sum(qty) as qty from mutasidetail ms join produk p on ms.idproduk = p.id where ms.idmutasi = $id group by ms.idproduk ");
            closedb();
            $i = 1;
            while ($rp = mysql_fetch_array($qp))
            {
              echo "<tr>
                    <td>$i</td>
                    <td>$rp[kode]</td>
                    <td>$rp[nama]</td>
                    <td>$rp[qty]</td>       
              </tr>";
            $i++;
            }
          ?>
          </tbody>
        </table>
      </div><!-- /.col -->
    </div><!-- /.row -->
   <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="?pg=appstok" class="btn btn-danger btn-xs"> Kembali</a>
      </div>
    </div>
  </section>
<?php
//Terima Barang
}else if(isset($_POST['terima'])){
  $id = $_POST['idmut'];
  opendb();
  querydb("insert into stock (idproduk,idgudang,idmutasi,stockin) select idproduk,idgudangtujuan,idmutasi,stockout from stock s join mutasi m on s.idmutasi = m.id where idmutasi = $id");
  querydb("update mutasi set status = 4, iduseraccept=$_SESSION[gudang],tglterima = now(),sj=$id where id = $id");
  closedb();
  echo "<div class='alert alert-success'>Success, Mutasi Gudang MAG$_POST[idmut] Sudah Diterima !</div>";
}else{
?>
<table class="table table-hover">
<tr>
  <th>No</th>
  <th>Mutasi</th>
  <th>Tanggal</th>
</tr>
<?php
opendb();
$qd = querydb("select m.idgudang,m.id,docno,namagudang,status,DATE_FORMAT(tglkirim,'%d %M %Y')as tanggal,DATE_FORMAT(tglterima,'%d %M %Y')as tglterima,keterangan from mutasi m
join gudang g
on m.idgudangtujuan = g.id where status = 3 or status = 4 and idgudangtujuan = $_SESSION[gudang] order by tanggal desc");
closedb();
$i = 1;
while($rs = mysql_fetch_array($qd))
{
if($rs['status'] == 3){
	 $ac = "<button type='submit' name='terima' class='btn btn-success btn-xs'><i class='fa  fa-sort-amount-asc'></i> Terima</button>";
}else if($rs['status'] == 4){
    $ac = "<button type='submit' name='detil' class='btn btn-default btn-xs'><i class='fa  fa-sort-amount-asc'></i> Detail</button>";
}else{
	  $ac = " ";
}
echo "
<tr>
  <td>$i</td>
  <td><b>MAG$rs[id]</b><br>Surat Jalan : SJ$rs[id]<br>No Document : <b>$rs[docno]</b><br><b>Dari Gudang :".namagudang($rs['idgudang'])."</b><br><b>Gudang Tujuan :</b> $rs[namagudang]<br>".status($rs['status'])."</td>
  <td>Tanggal Pengiriman: $rs[tanggal]<br>
      Tanggal Terima : $rs[tglterima]<br>
      <form method='POST'><input type='hidden' name='idmut' value=$rs[id]>$ac</form></td>
</tr>";
$i++;
} ?>
</table>
<?php } ?>
</div><!-- /.box -->
</div>
</div>
</div>

  <ol class="breadcrumb">
    <li><a href="?pg=adduser"><i class="fa fa-database"></i> Master Data</a></li>
    <li class="active">Mutasi Gudang</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->
<div class="box box-success">
<div class="box-header with-border">
  <div class="pull-right">
      <?php
      if (isset($_POST['lanjutadd']) or isset($_POST['editmutasi']) or isset($_POST['addbrg']) ){
        echo "";
      }else {
      ?>
      <form method="POST"><button type="submit" name="addp" class='btn btn-success btn-xs'><i class='fa fa-plus-square'></i> Tambah Mutasi</button></form>
      <?php } ?>
  </div>
  <h3 class="box-title">Data Record Mutasi (Gudang) - <?php echo namagudang($_SESSION['gudang']); ?></h3>
</div><!-- /.box-header -->
 <div class="box-body">

  <?php
  //Detail invoice or Print
  if (isset($_POST['detailm'])){
     $id = $_POST['id'];

     opendb();
     $qm = querydb("select id,idgudangtujuan,sj,DATE_FORMAT(tglkirim,'%d %M %Y') as tglkirim,docno from mutasi where id = $id");
     closedb();

     $rm = mysql_fetch_array($qm);
  ?>
  <section class="invoice">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> PT MUTIARA
          <small class="pull-right"><?php echo date('d M Y'); ?></small>
        </h2>
      </div><!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <address>
          Dari: 
          <strong><?php echo namagudang($_SESSION['gudang']); ?></strong><br>
          Gudang Tujuan : <?php echo namagudang($rm['idgudangtujuan']);?><br>
          No. Document : <?php echo $rm['docno'];?><br>
          Surat Jalan : <b>SJ<?php echo $rm['id'];?></b><br>
          Tanggal Kirim : <?php echo $rm['tglkirim'];?><br>
        </address>
      </div><!-- /.col -->
    </div><!-- /.row -->
    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Produk</th>
              <th>Nama produk</th>
              <th>Qty</th>
            </tr>
          </thead>
          <tbody>
          <?php 
            opendb();
            $qp = querydb("select kode,nama,sum(qty) as qty from mutasidetail ms join produk p on ms.idproduk = p.id where ms.idmutasi = $id group by ms.idproduk ");
            closedb();
            $i = 1;
            while ($rp = mysql_fetch_array($qp))
            {
              echo "<tr>
                    <td>$i</td>
                    <td>$rp[kode]</td>
                    <td>$rp[nama]</td>
                    <td>$rp[qty]</td>       
              </tr>";
            $i++;
            }
          ?>
          </tbody>
        </table>
      </div><!-- /.col -->
    </div><!-- /.row -->
   <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="printmag.php?id=<?php echo $id; ?>" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-print"></i> Print</a>
      </div>
    </div>
  </section><!-- /.content -->
  <?php
  //Jika Cekout Stok
  }else if (isset($_POST['cekout'])){
  $id = $_POST['idmutasi'];
  opendb();
  querydb("update mutasi set status = 3 where id = $id");
  querydb("insert into stock (idproduk,idgudang,idmutasi,stockout) 
              select idproduk,m.idgudang,idmutasi,qty from mutasidetail md 
              join mutasi m on md.idmutasi = m.id
              where md.idmutasi = $id");
  closedb();
  echo "<div class='callout callout-success'><h4>Success !!</h4><p>Mutasi Gudang Berhasil ditambahkan !! <a href='?pg=mutasi'><button type='button' class='btn btn-danger btn-xs'>Kembali</button</a></p></div>";
  
  //Add Produk, Edit Mutasi, Add Barang
  }else if (isset($_POST['lanjutadd']) or isset($_POST['editmutasi']) or isset($_POST['addbrg']) or isset($_POST['dellist']) ){


  //Jika Add Produk Baru
  if(isset($_POST['lanjutadd']))
  {

    $id = lastmutasi()+1;
    opendb();
    querydb("insert into mutasi (docno,idgudang,idgudangtujuan,tanggal,admin,status,keterangan,tglkirim) 
             values('$_POST[nodok]',$_POST[fromgudang],$_POST[togudang],now(),'$_SESSION[user]',1,'$_POST[keterangan]','$_POST[tglkirim]') ");
    closedb();

  //Jika Edit Mutasi
  }else if (isset($_POST['editmutasi']))
  {
    $id = $_POST['id'];

  //Jika Add Barang
  }else if (isset($_POST['addbrg']))
  {
    $id = $_POST['id'];
    opendb();
    querydb("insert into mutasidetail (idmutasi,idproduk,qty) values($id,$_POST[idbrg],$_POST[jml]) ");
    closedb();

  }

  //Jika delet list
  else if (isset($_POST['dellist']))
  {
    $id = $_POST['id'];
    $iddetail = $_POST['iddetail'];
    opendb();
    querydb("delete from mutasidetail where id = $iddetail ");
    closedb();
  }

    opendb();
    $qlb = querydb("select idmutasi,id,idproduk,sum(qty) as jml from mutasidetail  where idmutasi =  $id group by idproduk");
    closedb();

?>
<div class="row">
  <!-- Input Barang -->
  <div class="col-md-7">
  <div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Data Barang</h3>
  </div>
   <div class="box-body">
      <?php 
      opendb();
      $qd = querydb("select m.id,m.docno,g.namagudang,DATE_FORMAT(m.tanggal,'%d %M %Y') as tglkirim from mutasi m join gudang g on m.idgudangtujuan = g.id  where m.id = $id");
      closedb();
      $rs = mysql_fetch_array($qd);
      ?>
      <blockquote>
        <p>No Mutasi : MAG<?php echo $rs['id']; ?></p>
        <small>No. Dokumen : <?php echo $rs['docno']; ?></small>
        <small>Gudang Tujuan : <?php echo $rs['namagudang']; ?></small>
        <small>Tanggal Kirim: <?php echo $rs['tglkirim']; ?></small>
      </blockquote>
      <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Kode</th>
          <th>Nama Barang</th>
          <th>Stok</th>
          <th>Jumlah</th>
          <th>Action</th>
        </tr>
        <?php
          $gudang = $_SESSION['gudang'];
          opendb();
          $qd = querydb("select s.idproduk,kode,nama,harga,sum(stockin) as instok,sum(stockout) as outstok,(sum(stockin) - sum(stockout)) as sisastok from stock s
          join produk p
          on s.idproduk = p.id
          where idgudang = $gudang
          group by kode");
          closedb();
          $i = 1;
          echo "";
          while($rs = mysql_fetch_array($qd)){
          echo "<tr>
                <td>$i</td>
                <td>$rs[kode]</td>
                <td>$rs[nama]</td>
                <td>$rs[sisastok]</td>
                <td><form method='POST'><input type='number' name='jml' class='form-control input sm' required='required'></td>
                <td>
                      <input type='hidden' name='idbrg' value = $rs[idproduk]>
                      <input type='hidden' name='id' value = $id>
                      <input class='btn btn-success btn-xs' type='submit' name='addbrg' value='+ Add'>
                </form></td>
                </tr>";
            $i++;
          }
        ?>
      </table>
    </div>
  </div>
  </div>
  <!-- /.Input Barang -->

  <!-- List Barang -->
  <div class="col-md-5">
  <div class="box box-success box-solid ">
  <div class="box-header with-border">
  <h3 class="box-title">List Barang</h3>
  </div>
  <div class="box-body">
    <ul class="nav nav-stacked">
    <?php
    while($rsl = mysql_fetch_array($qlb))
    {
        echo "<li><form method='POST'>".namabarang($rsl['idproduk'])." <span class='pull-right badge bg-default'>$rsl[jml] KG</span>
         <input type='hidden' name='id' value=$rsl[idmutasi]><input type='hidden' name='iddetail' value=$rsl[id]><button type='submit' class='btn btn-box-tool' name='dellist'><i class='fa fa-times'></i></button></form></li>";
    }
    ?>
    
    </ul>
  </div>
  </div>
  </div>
  <div class="col-md-12">
      <hr>
     <form method='POST'>
        <input type='hidden' name='idmutasi' value='<?php echo $id; ?>' >
        <input type='hidden' name='nodoc' value='<?php echo $_POST['docno']; ?>' >
        <input type='submit' name='cekout' class='btn btn-success btn-xs' value='Submit'> 
        <a href='?pg=mutasi'><input type='button' class='btn btn-danger btn-xs' value='Kembali'></a>
     </form>
  </div>
  <!--   //Insert Produk
  
  Form Dokumen Suplier -->
</div>
<?php
}else if(isset($_POST['addp'])){
?>
<form method="POST">
  <div class="box-body">
    <div class="row">
    <div class="col-md-6">
    <div class="form-group">
      <label>No. Dokumen</label>
      <input type="text" name="nodok" class="form-control input-sm" required="required">
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
      <label>Tanggal Kirim</label>
      <input type="date" class="form-control input-sm" name="tglkirim">
    </div>
    </div>
    <div class="col-md-3">
    <div class="form-group">
      <label>Dari Gudang</label>
      <input type="text" class="form-control input-sm" value="<?php echo namagudang($_SESSION['gudang']); ?>" disabled>
    </div>
    </div>
     <div class="col-md-3">
    <div class="form-group">
      <label>Ke Gudang</label>
      <select name="togudang" class="form-control input-sm">
        <?php
        opendb();
        $qd = querydb("select * from gudang where id <> $_SESSION[gudang] order by namagudang asc");
        closedb();
        while($rs = mysql_fetch_array($qd)){
          echo "<option value='$rs[id]'>$rs[namagudang]</option>";
        }
        ?>
      </select>
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
      <label>Keterangan</label>
      <textarea name='keterangan' class="form-control input-sm"></textarea>
    </div>
    </div>
  </div>
  </div><!-- /.box-body -->
  <div class="box-footer">
     <?php echo "<input type='hidden' name='fromgudang' value = $_SESSION[gudang]>"; ?>
     <button type="submit" name="lanjutadd" class="btn btn-success btn-xs"> Lanjutkan </button> <a href="?pg=mutasi"><button type="button" name="input" class="btn btn-danger btn-xs"> Batal</button></a>
  </div><!-- /.box-footer -->
</form>
<?php
}else{
?>
<table class="table table-hover">
<tr>
  <th>No</th>
  <th>Mutasi</th>
  <th>Tanggal</th>
  <th>Admin</th>
  <th><center>Action</center></th>
</tr>
<?php
opendb();
$qd = querydb("select m.tglkirim,m.tglterima,m.id,docno,namagudang,m.status,DATE_FORMAT(tanggal,'%d %M %Y') as tanggal,admin,keterangan from mutasi m
join gudang g
on m.idgudangtujuan = g.id where idgudang = $_SESSION[gudang]");
closedb();
$i = 1;
while($rs = mysql_fetch_array($qd))
{
if($rs['status'] == 1 or $rs['status'] == 2 ){
  $ac = "<center><button type='submit' name='editmutasi' class='btn btn-default btn-xs'><i class='fa fa-edit (alias)'></i> Edit</button> 
        <button class='btn btn-danger btn-xs'>x Delete</button></center>";
}else if($rs['status'] == 3){
  $ac = "<center><button type='submit' name='detailm' class='btn btn-danger btn-xs' disabled>Waiting Approve</button></center> ";
}else{
  $ac = "<center><button type='submit' name='detailm' class='btn btn-default btn-xs'><i class='fa fa-edit (alias)'></i> Detail</button></center> ";
}
echo "
<tr>
  <td>$i</td>
  <td><b>MAG$rs[id]</b><br><b>Surat Jalan : SJ$rs[id]</b><br>No Document : <b>$rs[docno]</b><br> <b>Gudang Tujuan :</b> $rs[namagudang] <br>Status : ".status($rs['status'])."</td>
  <td>Tanggal Pengiriman: $rs[tglkirim]<br>Tanggal Terima: $rs[tglterima]</td>
  <td>$rs[admin]</td>
  <td>
      <form method='POST'>
        <input type='hidden' name='id' value = $rs[id]>
        <input type='hidden' name='doc' value = $rs[docno]>
        <input type='hidden' name='namagudang' value = '$rs[namagudang]'>
        <input type='hidden' name='tanggal' value = '$rs[tanggal]'>
        $ac
       </form>
  </td>
</tr>";
$i++;
} ?>
</table>
<?php } ?>  
</div><!-- /.box -->
</div>
</div>
</div>
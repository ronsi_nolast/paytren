
  <ol class="breadcrumb">
    <li><a href="?pg=adduser"><i class="fa fa-user"></i> Administrator</a></li>
    <li class="active">Add user</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->
<div class="box box-success">
<div class="box-header with-border">
  <div class="pull-right">
      <form method="POST"><button type="submit" name="adduser" class='btn btn-success btn-xs'><i class='fa fa-plus-square'></i> Tambah User</button></form>
  </div>
  <h3 class="box-title">Tabel User</h3>
</div><!-- /.box-header -->
<div class="box-body">
<?php
//Form Input User
if (isset($_POST['adduser']) or isset($_POST['inputuser'])){

  //Jika Input user
  if (isset($_POST['inputuser'])){
      if($_POST['pass'] <> $_POST['upass']){
        echo "<div class='alert alert-danger'>Failed, Password Belum Valid !!</div>";
      }else{
        opendb();
        querydb("insert into t_user (username,password,tglinput,idgudang,level,inby) values('$_POST[username]',md5('$_POST[pass]'),now(),$_POST[gudang],2,'$_SESSION[user]') ");
        closedb();
        echo "<div class='alert alert-success'>Success, User Berhasil Diinput !!</div>";
      }
  }
?>
<form class="form-horizontal" method="POST">
    <div class="form-group">
      <label class="col-sm-2 control-label">Username</label>
      <div class="col-sm-6">
        <input type="text" name='username' class="form-control input-sm"  placeholder="Username" required="required">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Password</label>
      <div class="col-sm-6">
        <input type="password" name='pass' class="form-control input-sm"  placeholder="Isikan Password" required="required">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Ulangi Password</label>
      <div class="col-sm-6">
        <input type="password" name='upass' class="form-control input-sm"  placeholder="Ulangi Password" required="required">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">User Gudang</label>
      <div class="col-sm-4">
        <select name="gudang" class="form-control input-sm" required="required">
          <?php
            opendb();
            $qd = querydb("select * from gudang order by namagudang asc");
            closedb();
            while ($rs = mysql_fetch_array($qd)){
                echo "<option value='$rs[id]'>$rs[namagudang]</option>";
            }
          ?>
        </select>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" name="inputuser" class="btn btn-success btn-xs">+ Submit </button> 
      <a href="?pg=adduser"><button type="button" name="input" class="btn btn-danger btn-xs"> x Batal</button></a>
    </div>
</form>
<?php 
}
?>
<br>
<table class="table table-hover">
<tr>
  <th>No</th>
  <th>Username</th>
  <th>User Gudang</th>
  <th>Tanggal Aktiv</th>
  <th>Action</th>
</tr>
<?php
	opendb();
	$qd = querydb("select username,namagudang,u.tglinput from t_user u left join gudang g on u.idgudang = g.id order by username asc");
	closedb();
	$i = 1;
	while($rs = mysql_fetch_array($qd))
	{
		echo "
			<tr>
  				<td>$i</td>
  				<td>$rs[username]</td>
  				<td>$rs[namagudang]</td>
          <td>$rs[tglinput]</td>
  				<td><button class='btn btn-default btn-xs'><i class='fa fa-edit (alias)'></i> Edit</button> <button class='btn btn-danger btn-xs'>x Delete</button></td>
			</tr>";
		$i++;
	} ?>
</table>
<hr>
</div>
</div><!-- /.box -->
</div>
</div>
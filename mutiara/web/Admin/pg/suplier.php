
  <ol class="breadcrumb">
    <li><a href="?pg=adduser"><i class="fa fa-database"></i> Master Data</a></li>
    <li class="active">Data Suplier</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->
<div class="box box-success">
<div class="box-header with-border">
  <div class="pull-right">
      <form method="POST"><button type="submit" name="addp" class='btn btn-success btn-xs'><i class='fa fa-plus-square'></i> Tambah Suplier</button></form>
  </div>
  <h3 class="box-title">Data Suplier</h3>
</div><!-- /.box-header -->
<div class="box-body">
<?php
if(isset($_POST['inputagen'])){
$ka = acaka(5);
opendb(); 
querydb("insert into suplier (kode,nama,alamat,hp,bank,rek,atasnama,npwp,datereg,admin,kota) 
         values('SP$ka','$_POST[nama]','$_POST[alamat]','$_POST[hp]','$_POST[nbank]','$_POST[norek]','$_POST[anama]','$_POST[npwp]',now(),'$_SESSION[user]','$_POST[kota]')");
closedb();
echo "<div class='callout callout-success'><h4>Success !!</h4><p>Suplier Berhasil ditambahkan !! <a href='?pg=suplier'><button type='button' class='btn btn-danger btn-xs'>Kembali</button</a></p></div>";
?>
<?php
}else if(isset($_POST['addp'])){
?>
<form role="form" method="POST">
  <div class="box-body">
    <div class="row">
    <div class="col-md-6">
    <div class="form-group">
      <label>Nama Suplier</label>
      <input type="text" name="nama" class="form-control input-sm" placeholder="Nama suplier...">
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
      <label>Alamat</label>
      <input type="text" name="alamat" class="form-control input-sm" placeholder="Alamat lengkap ...">
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
      <label>HP</label>
      <input type="text" name="hp" class="form-control input-sm" placeholder="No. Hp ...">
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
      <label>Kota/Kabupaten</label>
      <select name="kota" class="form-control input-sm">
      <?php
      opendb();
      $qd = querydb("select * from all_kabkot order by nama_kabkot asc");
      closedb();
      while($rs = mysql_fetch_array($qd)){
        echo "<option value='$rs[nama_kabkot]'>$rs[nama_kabkot]</option>";
      }
      ?>
      </select>
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
      <label>NPWP</label>
      <input type="text" name="npwp" class="form-control input-sm" placeholder="Npwp ...">
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
      <label>No.Rek Bank</label>
      <input type="text" name="norek" class="form-control input-sm" placeholder="No. Rekening Bank ...">
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
      <label>Nama Bank</label>
      <input type="text" name="nbank" class="form-control input-sm" placeholder="Nama Bank ...">
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
      <label>Atas Nama</label>
      <input type="text" name="anama" class="form-control input-sm" placeholder="Atas Nama ...">
    </div>
    </div>
  </div>
  </div><!-- /.box-body -->
  <div class="box-footer">
    <button type="submit" name="inputagen" class="btn btn-success btn-xs">+ Submit </button> <a href="?pg=suplier"><button type="button" name="input" class="btn btn-danger btn-xs"> x Batal</button></a>
  </div><!-- /.box-footer -->
</form>
<?php
}else{
?>
<div class="box-body table-responsive no-padding">
<table class="table table-hover">
<tr>
  <th>No</th>
  <th>Kode</th>
  <th>Nama</th>
  <th>Alamat</th>
  <th>Data Bank</th>
  <th>NPWP</th>
  <th>Action</th>
</tr>
<?php
opendb();
$qd = querydb("select * from suplier ");
closedb();
$i = 1;
while($rs = mysql_fetch_array($qd))
{
echo "
<tr>
  <td>$i</td>
  <td>$rs[kode]</td>
  <td>$rs[nama]</td>
  <td>$rs[alamat]</td>
  <td>$rs[bank] - $rs[rek]<br>$rs[atasnama]</td>
  <td>$rs[npwp]</td>
  <td><button class='btn btn-default btn-xs'><i class='fa fa-edit (alias)'></i> Edit</button> <button class='btn btn-danger btn-xs'>x Delete</button></td>
</tr>";
$i++;
} ?>
</table>
<?php } ?>  
</div><!-- /.box -->
</div>
</div>
</div>
</div>

  <ol class="breadcrumb">
    <li><a href="?pg=adduser"><i class="fa fa-database"></i> Master Data</a></li>
    <li class="active">Data Gudang</li>
  </ol>
</section>
<br><br>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->
<div class="box box-success">
<div class="box-header with-border">
  <div class="pull-right">
      <form method="POST"><button type="submit" name="addgudang" class='btn btn-success btn-xs'><i class='fa fa-plus-square'></i> Tambah Gudang</button></form>
  </div>
  <h3 class="box-title">Data Gudang</h3>
</div><!-- /.box-header -->
<div class="box-body">
<?php
//Insert Gudang
if(isset($_POST['inputgdg'])){
  $kg = acaka(5);
  opendb();
  querydb("insert into gudang (kodegudang,namagudang,alamat,kota,tglinput,inby) values('KG$kg','$_POST[ngudang]','$_POST[alamat]','$_POST[kota]',now(),'$_SESSION[user]')");
  closedb();
  echo "<div class='callout callout-success'><h4>Success !!</h4><p>Data Gudang Berhasil ditambahkan !!</p></div>";
?>
<?php
//Form Input Gudang
}else if (isset($_POST['addgudang'])){
?>
<form class="form-horizontal" method="POST">
    <div class="form-group">
      <label class="col-sm-2 control-label">Nama Gudang</label>
      <div class="col-sm-6">
        <input type="text" name='ngudang' class="form-control input-sm"  placeholder="nama gudang" required="required">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Alamat Gudang</label>
      <div class="col-sm-10">
        <input type="text" name='alamat' class="form-control input-sm"  placeholder="Isikan alamat gudang" required="required">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Kota/Kabupaten</label>
      <div class="col-sm-6">
        <select name="kota" class="form-control input-sm">
          <?php
            opendb();
            $qd = querydb("select * from all_kabkot order by nama_kabkot asc");
            closedb();
            while ($rs = mysql_fetch_array($qd)){
                echo "<option value='$rs[nama_kabkot]'>$rs[nama_kabkot]</option>";
            }
          ?>
        </select>
      </div>
    </div>
   <div class="box-footer">
      <button type="submit" name="inputgdg" class="btn btn-success btn-xs">+ Submit </button> 
      <a href="?pg=gudang"><button type="button" name="input" class="btn btn-danger btn-xs"> x Batal</button></a>
    </div>
</form>
<?php 
}
?>
<hr>
<table class="table table-hover">
<tr>
  <th>No</th>
  <th>kode Gudang</th>
  <th>Nama Gudang</th>
  <th>Alamat</th>
  <th>Kota/Kabupaten</th>
  <th>Action</th>
</tr>
<?php
opendb();
$qd = querydb("select * from gudang");
closedb();
$i = 1;
while($rs = mysql_fetch_array($qd))
{
echo "
<tr>
  <td>$i</td>
  <td>$rs[kodegudang]</td>
  <td>$rs[namagudang]</td>
  <td>$rs[alamat]</td>
  <td>$rs[kota]</td>
  <td><button type='submit' name='editgudang' class='btn btn-default btn-xs'><i class='fa fa-edit (alias)'></i> Edit </button> <button class='btn btn-danger btn-xs'>x Delete</button></td>
</tr>";
$i++;
} ?>
</table>
</div><!-- /.box -->
</div>
</div>
</div>
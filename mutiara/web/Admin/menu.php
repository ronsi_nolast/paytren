<?php
$kodegudang = kodegudang($_SESSION['gudang']);
opendb();
$qd = querydb("select count(id) as id from mutasi where status = 3");
$qb = querydb("select count(id) as id from belanja where status = 2");
$qg = querydb("select count(spkbno) as id from spkb where status = 1 and kodegudang = '$kodegudang' ");
closedb();
$rs = mysql_fetch_array($qd);
$rb = mysql_fetch_array($qb);
$rg = mysql_fetch_array($qg);
?>
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
 
  <li class ="<?php if($_GET['pg']=='home') echo "active treeview"; ?>" ><a href="?pg=home"><i class="fa fa-dashboard"></i> Dashboard</a></li>
 
  <li class="<?php if($_GET['pg']=='adduser') echo "active treeview"; ?>"><a href="#"><i class="fa fa-user"></i><span>Administrator</span><i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li "<?php if($_GET['pg']=='adduser') echo 'class ="active"'; ?>" ><a href="?pg=adduser"><i class="fa fa-user-plus"></i> Add User</a></li>
    </ul>
  </li>

  <li class="<?php if($_GET['pg']=='regagen') echo "active treeview"; ?>"><a href="#"><i class="fa fa-registered"></i><span>Report</span><i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li "<?php if($_GET['pg']=='regagen') echo 'class ="active"'; ?>" ><a href="?pg=regagen"><i class="fa fa-user-secret"></i>Report Belanja Agen</a></li>
    </ul>
  </li>
  <li class ="<?php if($_GET['pg']=='belanjaagen') echo "active treeview"; ?>" ><a href="?pg=belanjaagen"><i class="fa fa-shopping-cart"></i> Belanja Agen<span class="label label-primary pull-right"><?php echo $rb['id']; ?></span></a> </li>
  <li class="<?php if($_GET['pg']=='spkb' or $_GET['pg']=='spkb') echo "active treeview"; ?>"><a href="?pg=spkb"><i class="fa fa-file-archive-o"></i><span>SPKB</span><i class="fa fa-angle-left pull-right"></i><span class="label label-danger pull-right"><?php echo $rg['id']; ?></span></a>
  <li "<?php if($_GET['pg']=='allstok') echo 'class ="active"'; ?>" ><a href="?pg=allstok"><i class="fa fa-pie-chart"></i>Stok</a></li>
  <li class="<?php if($_GET['pg']=='koreksi' or $_GET['pg']=='instok' or $_GET['pg']=='mutasi' or $_GET['pg']=='appstok') echo "active treeview"; ?>"><a href="#"><i class="fa  fa-refresh"></i><span>Mutasi</span><span class="label label-success pull-right"><?php echo $rs['id']; ?></span></a> 
    <ul class="treeview-menu">
      <li><a href="?pg=koreksi"><i class="fa fa-cog"></i> Koreksi Stok</a></li>
      <li><a href="?pg=instok"><i class="fa fa-plus-square"></i> Input Stok</a></li>
      <li><a href="?pg=mutasi"><i class="fa fa-share-square-o"></i>Antar Gudang</a></li>
      <li><a href="?pg=appstok"><i class="fa  fa-sign-in"></i>Terima Mutasi (Gudang) <span class="label label-success pull-right"><?php echo $rs['id']; ?></span></a></li>
      <li></li>
    </ul>
  </li>
  <li class="<?php if($_GET['pg']=='kategori' or $_GET['pg']=='barang' or $_GET['pg']=='harga' or $_GET['pg']=='agen' or $_GET['pg']=='gudang'  or $_GET['pg']=='suplier'
  ) echo "active treeview"; ?>"> <a href="#"><i class="fa fa-database"></i><span>Master Data</span><i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="?pg=kategori"><i class="fa fa-houzz"></i> Data Kategori</a></li>
      <li><a href="?pg=barang"><i class="fa fa-archive"></i> Data Barang</a></li>
      <li><a href="?pg=harga"><i class="fa  fa-money"></i> Data Harga</a></li>
      <li><a href="?pg=gudang"><i class="fa fa-institution"></i> Data Gudang</a></li>
      <li><a href="?pg=suplier"><i class="fa fa-users"></i> Data Supplier</a></li>
      <li><a href="?pg=agen"><i class="fa fa-user-secret"></i> Data Agen</a></li>
      <li><a href="?pg=konsumen"><i class="fa fa-user"></i> Data Konsumen</a></li>      
    </ul>
  </li>
  <li><a href="logout.php"><i class="fa fa-ban"></i> Logout</a></li>

</ul>